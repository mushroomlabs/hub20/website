---
Title: "About Hub20"
---

In one sentence: *Hub20 is an open source, self-hosted payment processor
for Ethereum and EVM-compatible networks.*

It works as a web application that can interact with network that is
compatible with [Ethereum](https://ethereum.org)'s JSON RPC API. It
can keep track of any type of token transfer between two different
parties. It hides all of the complexity from end-users so that they
can make and receive payments in the blockchains native currency or
ERC20-compliant token. It abstracts away all the details and provides
the fastest/cheapest possible route to make it easy for people to
accept cryptocurrencies as payment online.

In practice, one can think of each instance of Hub20 of a single
"crypto bank" where users simply have access to their individual
accounts and token balances, while the person running the deployment
(hub operator or hub owner) is the one with actual access to the bank
vault and funds.

## Features

For users who just want a simple way to make cheap/fast transfers with crypto:

 * Direct, peer-to-peer payments for any Ethereum-compatible token.
 * No transaction fees for transfers between accounts of the same instance
 * Payments are always sent using the cheapest route possible

For merchants:

 * No transaction fees for accepting payments
 * No KYC
 * Easy to integrate checkout widget on third party sites, lets you
   accept crypto payments via blockchain or "layer-2" networks - such
   as [Raiden](https://raiden.network)

For developers:

 * Pluggable architecture for managing Ethereum accounts: you can use
   standard keystore accounts, HD Wallets or implement your own
   method.
 * Easy to extend and add modules - e.g, hub operators can aggregate
   user deposits and add liquidity to Decentralized Exchanges and
   possibly earning money for everyone


## Beyond payment processing

Hub20 can be used by individuals and businesses of any size and manage
any amount of funds. Its focus however stays on providing easy to use
functionality for small groups and communities whose peers know each
other and already have established trust among themselves.

 * E-commerce: easy to connect to websites and accept payments
 * Banking and bookkeeping for marketplaces: if you run an online or
   physical marketplace, you can use Hub20 as a way to give each
   merchant and each customer an account which can then be used to
   manage their balances and payouts
 * Small group banking: run an instance to manage expenses from your
   community club, keep track of house bills and split with housemates
 * Investment cooperative: users with little knowledge of De-Fi can
   work together with the hub operator and invest their pooled
   resources
